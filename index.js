(() => {
    const alphaTemplate = '{alpha}',
        defColorTemplate = `rgba(250, 10, 30, ${alphaTemplate})`,
        defColorAlpha = 0.9;

    const TWO_PI = 2 * Math.PI,
        canvas = document.getElementById('root'),
        ctx = canvas.getContext('2d');

    let w,h, mouse, dots;

    const config = {
        dotsMaxCount: 500,
        dotMinRad: 6,
        dotMaxRad: 20,
        sphereRad: 300,
        bigDotRad: 35,
        mouseSize: 100,
        massFactor: 0.002,
        defColor: defColorTemplate.replace(alphaTemplate, defColorAlpha), 
        smooth: 0.75
    }

    class Dot {
        constructor(rad) {
            this.pos = { x: mouse.x || 0, y: mouse.y || 0 };
            this.vel = { x: 0, y: 0 }; //speed
            this.rad = rad || random(config.dotMinRad, config.dotMaxRad);
            this.mass = this.rad * config.massFactor;
            this.color = config.defColor;
        }

        draw(x, y) {
            if(x) { this.pos.x = x; } 
            else { this.pos.x += this.vel.x; }

            if(y) { this.pos.y = y; } 
            else { this.pos.y += this.vel.y; }

            if(this.pos.x < 0) this.pos.x = 0;
            else if(this.pos.x > w) this.pos.x = w;

            if(this.pos.y < 0) this.pos.y = 0;            
            else if(this.pos.y > h) this.pos.y = h;

            createCircle(this.pos.x, this.pos.y, this.rad, true, this.color);
            createCircle(this.pos.x, this.pos.y, this.rad, false, config.defColor);
        }
    }

    function updateDots() {
        for(let i = 0; i < dots.length; i++) {
            let acc = { x: 0, y: 0 } //speed which depends on other dots

            for(let j = 0; j < dots.length; j++) {
                if(i == j) continue;

                let [a, b] = [dots[i], dots[j]],
                
                    delta = { x: b.pos.x - a.pos.x, y: b.pos.y - a.pos.y }, //delta between positions of dots
                    dist = Math.sqrt( delta.x * delta.x + delta.y * delta.y ) || 1, // distanse between dots
                    force = (dist - config.sphereRad)/dist * b.mass;

                if(j == 0) {
                    let alpha = config.mouseSize / dist;

                    a.color =  defColorTemplate.replace(alphaTemplate, alpha);

                    if(dist < config.mouseSize) {
                        force = (dist - config.mouseSize) * b.mass
                    } else {
                        force = a.mass * config.smooth;
                    }
                }

                acc.x += delta.x * force;
                acc.y += delta.y * force;
            }
            
            dots[i].vel.x = dots[i].vel.x * config.smooth + acc.x * dots[i].mass;
            dots[i].vel.y = dots[i].vel.y * config.smooth + acc.y * dots[i].mass;
        }


        dots.map(e => e == dots[0] ? e.draw(mouse.x, mouse.y) : e.draw());
    }

    function createCircle(x, y, rad, fill, color) {
        ctx.fillStyle = ctx.strokeStyle = color;
        ctx.beginPath();
        ctx.arc(x, y, rad, 0, TWO_PI);
        ctx.closePath();
        fill ? ctx.fill() : ctx.stroke();
    }

    function random(min, max) {
        return Math.random() * (max - min) + min;
    }

    function init() {
        w = canvas.width = innerWidth;
        h = canvas.height = innerHeight;

        mouse = { x: w/2, y: h/2, down: false };
        dots = [];

        dots.push(new Dot(config.bigDotRad));
    }

    function loop() {
        ctx.clearRect(0, 0, w, h);

        if(mouse.down) {
            dots.push(new Dot());
        }

        if(dots.length > config.dotsMaxCount) {
            let removeCount = dots.length - config.dotsMaxCount;

            dots.splice(1, removeCount);
        }

        updateDots();

        window.requestAnimationFrame(loop);
    }

    init();
    loop();

    function setPos({ layerX, layerY}) {
        [mouse.x, mouse.y] =  [layerX, layerY];
    }

    function isDown() {
        mouse.down = !mouse.down;
    }

    canvas.addEventListener('mousemove', setPos);
    window.addEventListener('mousedown', isDown);
    window.addEventListener('mouseup', isDown);

})()